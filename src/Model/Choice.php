<?php

namespace App\Model;

class Choice
{
    /** @var string */
    private $text;

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return Choice
     */
    public function setText(string $text): self
    {
        $this->text = $text;
        return $this;
    }
}