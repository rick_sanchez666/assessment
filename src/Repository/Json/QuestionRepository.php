<?php

namespace App\Repository\Json;

use App\Repository\QuestionRepositoryInterface;
use App\Service\JsonDataAdapter;
use Symfony\Component\Serializer\SerializerInterface;

class QuestionRepository implements QuestionRepositoryInterface
{

    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var JsonDataAdapter
     */
    private $dataAdapter;

    public function __construct(SerializerInterface $serializer, JsonDataAdapter $dataAdapter)
    {
        $this->serializer = $serializer;
        $this->dataAdapter = $dataAdapter;
    }

    public function getAllQuestions()
    {
        $data = $this->dataAdapter->getData();

        return $data;
    }
}