<?php

namespace App\Repository;


interface QuestionRepositoryInterface
{
    public function getAllQuestions();
}