<?php
namespace App\Service;


interface DataAdapter
{
    public function getData();
}