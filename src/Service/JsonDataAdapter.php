<?php

namespace App\Service;

use Symfony\Component\Finder\Finder;

class JsonDataAdapter implements DataAdapter
{
    public function __construct()
    {
    }

    public function getData()
    {
        $finder = new Finder();

        $files = $finder->in('var/data/*')->name('*.json');
        $data = file_get_contents($files);

        return json_decode($data, true);
    }
}